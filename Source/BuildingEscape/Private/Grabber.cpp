// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	SetupInputComponent();

	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle)
	{

	}
	
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector LineTraceEnd = GetReachLineEnd();

	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(LineTraceEnd);
	}
	
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	FVector LineTraceEnd = GetReachLineEnd();
	//DrawDebugLine(GetWorld(), pLocation, LineTraceEnd, FColor(255, 0, 0), false, 0.0f, 0.0f, 5.0f);

	//Line trace (RAY CAST)
	FCollisionQueryParams TraceParameters = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	FHitResult LineTraceHit;
	GetWorld()->LineTraceSingleByObjectType(LineTraceHit, GetReachLineStart(), LineTraceEnd, FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), TraceParameters);

	return LineTraceHit;
}

void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Release);
	}
}

FVector UGrabber::GetReachLineEnd() const
{
	FVector pLocation;
	FRotator pRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(pLocation, pRotation);

	return pLocation + Reach * pRotation.Vector();
}

FVector UGrabber::GetReachLineStart() const
{
	FVector pLocation;
	FRotator pRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(pLocation, pRotation);

	return pLocation;
}

void UGrabber::Grab()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab action !"))
	FHitResult HitResult = GetFirstPhysicsBodyInReach();

	if (HitResult.GetActor())
	{
		UE_LOG(LogTemp, Warning, TEXT("Ray collision with %s"), *HitResult.GetActor()->GetName())
		PhysicsHandle->GrabComponent(HitResult.GetComponent(), NAME_None, HitResult.GetComponent()->GetOwner()->GetActorLocation(), true);
	}
}

void UGrabber::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Release action !"))
	PhysicsHandle->ReleaseComponent();
}

